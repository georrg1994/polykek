package com.twins.argument.polykek.firebase

import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.twins.argument.polykek.Application
import com.twins.argument.polykek.R

open class FireBaseActivity : AppCompatActivity() {
    protected var firebase: FirebaseHelper = Application.firebase
    protected var auth: FirebaseAuth = Application.auth


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }
}