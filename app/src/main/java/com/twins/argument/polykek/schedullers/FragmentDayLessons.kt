package com.twins.argument.polykek.schedullers

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.HelpFunctions
import com.twins.argument.polykek.Utils.LIST_OF_LESSONS
import com.twins.argument.polykek.Utils.PagerFragmentAdapter
import com.twins.argument.polykek.models.Lesson
import kotlinx.android.synthetic.main.fragment_weekdays.*
import kotlin.collections.ArrayList


/**
 * Created by darkt on 8/24/2017.
 */
class FragmentDayLessons : Fragment(){
    private var dayFragments: ArrayList<FragmentListOfLessons> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_weekdays, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagerAdapter = PagerFragmentAdapter(childFragmentManager)

        val daysName = arrayOf("Пн","Вт","Ср","Чт","Пт","Сб")
        for(i in 0 .. 5){
            val dayFragment = FragmentListOfLessons()
            val arg = Bundle()
            arg.putParcelableArrayList(LIST_OF_LESSONS, getRandomLessonsList())
            dayFragment.arguments = arg
            dayFragments.add(dayFragment)
            pagerAdapter.addFragmentTitlePair(i, dayFragment, daysName[i])
        }

        viewPagerView.adapter = pagerAdapter
        viewPagerView.offscreenPageLimit = 3
        tabLayout.setupWithViewPager(viewPagerView)
    }

    private fun getRandomLessonsList(): ArrayList<Lesson>{
        val lessonsList = ArrayList<Lesson>()
        val timeList = arrayOf("8:00-9:30", "10:00-11:30", "12:00-13:30", "14:00-15:30", "16:00-15:30")
        for(i in 0 .. 4)
            if(HelpFunctions.rand(1, 5) % (i + 1) != 0)
                lessonsList.add(HelpFunctions.getRandomLesson(timeList[i]))

        return lessonsList
    }
}