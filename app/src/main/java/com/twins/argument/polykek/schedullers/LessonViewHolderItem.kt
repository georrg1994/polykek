package com.twins.argument.polykek.schedullers

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.twins.argument.polykek.databinding.ItemLessonBinding
import com.twins.argument.polykek.models.Lesson


class LessonViewHolderItem private constructor(private val binding: ItemLessonBinding) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n")
    fun bindTo(lesson: Lesson) {
        binding.timeField.text = lesson.time
        binding.address.text = lesson.address
        binding.teacherName.text = lesson.teacherName
        binding.title.text = lesson.lessonName
        binding.executePendingBindings()
    }

    companion object {
        fun create(inflater: LayoutInflater,
                   parent: ViewGroup): LessonViewHolderItem {
            val binding = ItemLessonBinding.inflate(inflater, parent, false)
            return LessonViewHolderItem(binding)
        }
    }
}