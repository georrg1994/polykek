package com.twins.argument.polykek.authorization

import android.content.Intent
import android.os.Bundle
import android.app.ActivityOptions
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.twins.argument.polykek.MainActivity
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.*
import com.twins.argument.polykek.firebase.FireBaseActivity
import com.twins.argument.polykek.models.User
import kotlinx.android.synthetic.main.activity_sign_up.*

class ActivitySignUp : FireBaseActivity() {

    private lateinit var firstName: String
    private lateinit var secondName: String
    private lateinit var email: String
    private lateinit var password: String
    private lateinit var groupId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()

        signUpButton.setOnClickListener{ createNewUser() }
        backButton.setOnClickListener{ onBackPressed() }
    }

    private fun createNewUser() {
        firstName = firstNameField.text.toString().trim()
        secondName = secondNameField.text.toString().trim()
        email = emailField.text.toString().trim()
        password = passwordField.text.toString().trim()
        groupId = groupIdField.text.toString().trim()


        if (!validateForm(firstName, secondName, email, password))
            return

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, { task ->
            if (task.isSuccessful)
                onAuthSuccess(task.result.user)
            else
                Toast.makeText(this@ActivitySignUp, "Error sign up", Toast.LENGTH_SHORT).show()
        })
    }

    private fun onAuthSuccess(firebaseUser: FirebaseUser) {
        firebase.fetchUserById(firebaseUser.uid).first().subscribe({ user2 ->

            var user = user2
            val isNew = user == null

            if (isNew)
                user = User(firstName, secondName, email, groupId)

            val sharedPrefEditor = HelpFunctions.getSharedPrefEditor(this)
            sharedPrefEditor.putString(USER_ID, auth.currentUser?.uid!!)
            sharedPrefEditor.putString(USER_FIRST_NAME, user.firstName)
            sharedPrefEditor.putString(USER_LAST_NAME, user.lastName)
            sharedPrefEditor.putString(USER_EMAIL, user.email)
            sharedPrefEditor.putString(USER_GROUP, user.groupId)
            sharedPrefEditor.putString(USER_AVATAR, user.avatar)
            sharedPrefEditor.apply()


            if (isNew) {
                user.id = firebaseUser.uid
                firebase.pushNewUser(user)
            }

            // Go to MainActivity
            val intent = Intent(this@ActivitySignUp, MainActivity::class.java)
            val options = ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
            startActivity(intent, options.toBundle())

            finish()
        })
    }

    private fun fieldIsValid(editText: EditText, field: String, errorMsg: String): Boolean =
            if (field.isEmpty()) {
                editText.error = errorMsg
                false
            } else {
                editText.error = null
                true
            }


    private fun validateForm(firstName: String, secondName: String, email: String,
                             password: String): Boolean{

        return fieldIsValid(firstNameField, firstName, "введите имя") &&
            fieldIsValid(secondNameField, secondName, "введите фаммилию") &&
            fieldIsValid(emailField, email, "введите почту") &&
            fieldIsValid(passwordField, password, "введите пароль")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_animation_enter, R.anim.left_animation_leave)
    }
}
