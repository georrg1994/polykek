package com.twins.argument.polykek.messages

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.twins.argument.polykek.Application.Companion.firebase
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.CHAT_ID
import com.twins.argument.polykek.Utils.HelpFunctions
import com.twins.argument.polykek.Utils.RECIPIENT
import com.twins.argument.polykek.databinding.FragmentChatBinding
import com.twins.argument.polykek.firebase.FirebaseFragment
import com.twins.argument.polykek.models.Comment
import com.twins.argument.polykek.models.User
import java.util.*


class ChatFragment : FirebaseFragment() {
    private lateinit var binding: FragmentChatBinding
    private var chatId: String = "chat-zombie"
    private var adapter: CommentsAdapter? = null
    private var listener: KeyboardListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments != null)
            chatId = arguments!!.getString(CHAT_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.commentEditTextView.inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

        adapter = CommentsAdapter(firebase.getMessagesRef(chatId))
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context)

        adapter!!.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                binding.recyclerView.smoothScrollToPosition(positionStart)
            }
        })

        val commentSendBtnView = view.findViewById<View>(R.id.commentSendBtnView)
        commentSendBtnView.setOnClickListener{ createComment(binding.commentEditTextView.text.toString()) }
        binding.colorLayout.setOnClickListener{ createComment(binding.commentEditTextView.text.toString()) }

        binding.commentEditTextView.visibility = View.VISIBLE
        commentSendBtnView.visibility = View.VISIBLE
    }

    private fun createComment(message: String) {
        val newMessage = message.replace("(\r\n|\n)".toRegex(), " ").trim { it <= ' ' }
        val messageStringLength = newMessage.trim { it <= ' ' }.length
        if (messageStringLength == 0) {
            Toast.makeText(context, "Please type some text", Toast.LENGTH_SHORT).show()
            return
        }
        binding.commentEditTextView.setText("")

        val comment = Comment(auth.currentUser!!.uid, HelpFunctions.getCurrentUserFullName(context!!), newMessage, Date().time)
        firebase.createPrivateMessage(comment, arguments?.getParcelable(RECIPIENT) as User)
        listener?.hideKeyBoard()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is KeyboardListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        fun newInstance(chatId: String): Fragment {
            val args = Bundle()
            args.putString(CHAT_ID, chatId)
            val fragment = ChatFragment()
            fragment.arguments = args
            return fragment
        }

        fun newInstance(recipient: User): Fragment {
            val args = Bundle()
            args.putParcelable(RECIPIENT, recipient)
            val instance = ChatFragment()
            instance.arguments = args
            return instance
        }
    }
}