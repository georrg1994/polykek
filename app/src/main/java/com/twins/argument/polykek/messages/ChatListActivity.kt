package com.twins.argument.polykek.messages

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import com.twins.argument.polykek.MainActivity
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.*
import com.twins.argument.polykek.firebase.FireBaseActivity
import com.twins.argument.polykek.models.Chat
import com.twins.argument.polykek.models.User
import kotlinx.android.synthetic.main.activity_chat_list.*
import rx.Subscription
import rx.subscriptions.CompositeSubscription

interface KeyboardListener {
    fun hideKeyBoard()
}

class ChatListActivity : FireBaseActivity(), KeyboardListener {
    private val mCompositeSubscription = CompositeSubscription()
    private lateinit var adapter: ChatsAdapter
    private var chatsUpdatesSubscription: Subscription? = null
    private var chatId: String = "chatId"
    private var items: ArrayList<Chat> = ArrayList()

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, ChatListActivity::class.java)
            val options = ActivityOptions.makeCustomAnimation(context, R.anim.fade_out, R.anim.fade_in)
            context.startActivity(intent, options.toBundle())
        }

        fun startActivity(context: Context, recipient: User) {
            val intent = Intent(context, ChatListActivity::class.java)
            intent.putExtra(NEW_MESSAGE, true)
            intent.putExtra(USER, recipient)
            val options = ActivityOptions.makeCustomAnimation(context, R.anim.fade_out, R.anim.fade_in)
            context.startActivity(intent, options.toBundle())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)

        if (intent.getBooleanExtra(NEW_MESSAGE, false)) {
            val recipient = intent.getParcelableExtra<User>(USER)
            chatId = firebase.createPrivateChat(this, recipient)
        }

        // Init header
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Сообщения"

        recyclerView.layoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)

        // firebase listener
        chatsUpdatesSubscription = firebase.fetchChatsEvent().subscribe{event ->
            val chat = event.value
            chat.id = event.key

            when{
                event.eventType === RxFirebaseChildEvent.EventType.ADDED -> {
                    adapter.addNewChat(chat)
                    recyclerView.scrollToPosition(adapter.itemCount - 1)
                }
                event.eventType === RxFirebaseChildEvent.EventType.CHANGED -> {
                    adapter.updateChat(chat)
                }
                event.eventType === RxFirebaseChildEvent.EventType.REMOVED -> {
                    adapter.removeChat(chat.id)
                    recyclerView.scrollToPosition(0)
                }
            }
        }

        adapter = ChatsAdapter(this, items, object : ChatItemListener{
            override fun removeChat(chatId: String) {
                adapter.removeChat(chatId)
                firebase.removeChat(chatId)
            }

            override fun openChat(chatId: String) {
                showChat(adapter.getItem(chatId))
            }
        })
        recyclerView.adapter = adapter
    }


    fun showChat(chat: Chat) {
        for (user in chat.users.values)
            if (user.id != auth.currentUser!!.uid) {
                val chatFragment = ChatFragment.newInstance(user)
                val bundle = Bundle()
                bundle.putString(CHAT_ID, chat.id)
                bundle.putParcelable(RECIPIENT, user)
                chatFragment.arguments = bundle
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.chatDialogFragmentContainer, chatFragment)
                        .addToBackStack(null)
                        .commit()

                supportActionBar!!.title = user.getFullName()
                chatDialogFragmentContainer.visibility = View.VISIBLE
            }
    }


    override fun onDestroy() {
        super.onDestroy()
        mCompositeSubscription.clear()
        if(chatsUpdatesSubscription != null) {
            chatsUpdatesSubscription!!.unsubscribe()
            chatsUpdatesSubscription = null
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (chatDialogFragmentContainer.visibility == View.VISIBLE) {
            chatDialogFragmentContainer.visibility = View.GONE
            supportActionBar!!.title = "Messages"
            HelpFunctions.hideSoftKeyboard(this)
        }
    }

    override fun hideKeyBoard() {
        HelpFunctions.hideSoftKeyboard(this)
    }
}
