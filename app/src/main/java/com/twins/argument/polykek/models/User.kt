package com.twins.argument.polykek.models

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class User() : ParentItem(){
    var avatar: String = avatar_default
    var firstName: String = "user"
    var lastName: String = "user"
    var email: String = "email@email.com"
    var groupId: String = "9999.1"
    var studyingWordLvl: String = "Магистр"
    var studyingForm: String = "Очная"
    var studyingNumberLvl: String = "2"
    var university: String = "Институт компьютерных наук и технологий"
    var studyingDirection: String = "09.04.01 Информатика и вычислительная техника"
    var studyingDirectionProfile: String = "09.04.01_18 Встраиваемые системы управления"
    var department: String = "Кафедра компьютерных систем и программных технологий"
    var userStatus: String = "студент"

    constructor(firstName: String, lastName: String, email: String, groupId: String): this() {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.groupId = groupId
    }

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        firstName = parcel.readString()
        lastName = parcel.readString()
        email = parcel.readString()
        groupId = parcel.readString()
        avatar = parcel.readString()
        studyingWordLvl = parcel.readString()
        studyingForm = parcel.readString()
        studyingNumberLvl = parcel.readString()
        university = parcel.readString()
        studyingDirection = parcel.readString()
        studyingDirectionProfile = parcel.readString()
        department = parcel.readString()
        userStatus = parcel.readString()
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        val result = HashMap<String, Any?>()
        result["firstName"] = firstName
        result["lastName"] = lastName
        result["email"] = email
        result["groupId"] = groupId
        result["avatar"] = avatar
        result["studyingWordLvl"] = studyingWordLvl
        result["studyingForm"] = studyingForm
        result["studyingNumberLvl"] = studyingNumberLvl
        result["university"] = university
        result["studyingDirection"] = studyingDirection
        result["studyingDirectionProfile"] = studyingDirectionProfile
        result["department"] = department
        result["userStatus"] = userStatus
        return result
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(firstName)
        dest.writeString(lastName)
        dest.writeString(email)
        dest.writeString(groupId)
        dest.writeString(avatar)
        dest.writeString(studyingWordLvl)
        dest.writeString(studyingForm)
        dest.writeString(studyingNumberLvl)
        dest.writeString(university)
        dest.writeString(studyingDirection)
        dest.writeString(studyingDirectionProfile)
        dest.writeString(department)
        dest.writeString(userStatus)
    }

    fun getFullName(): String{
        return "$firstName $lastName"
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        var avatar_default: String = "https://firebasestorage.googleapis.com/v0/b/polykek-1.appspot." +
                "com/o/avatars%2F14847276461439919867_bryus-vsemoguschiy_3.jpg?" +
                "alt=media&token=7d53b349-4511-4e16-ad36-406e0dc2f014" // Jim kerry
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}