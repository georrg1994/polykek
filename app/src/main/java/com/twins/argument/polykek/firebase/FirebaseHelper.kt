package com.twins.argument.polykek.firebase

import android.content.Context
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import com.kelvinapps.rxfirebase.RxFirebaseDatabase
import com.twins.argument.polykek.Application.Companion.auth
import com.twins.argument.polykek.Utils.*
import com.twins.argument.polykek.Utils.HelpFunctions.Companion.getPrivateChatId
import com.twins.argument.polykek.models.Chat
import com.twins.argument.polykek.models.Comment
import com.twins.argument.polykek.models.User
import rx.Observable
import java.util.*

private const val USERS: String = "users"

class FirebaseHelper{

    private val database = FirebaseDatabase.getInstance().reference

    fun fetchUserById(userId: String): Observable<User> {
        return RxFirebaseDatabase
                .observeSingleValueEvent(database.child(USERS).child(userId), User::class.java)
    }

    fun pushNewUser(user: User) {
        val childUpdates = HashMap<String, Any>()
        childUpdates["$USERS/${user.id}"] = user.toMap()
        database.updateChildren(childUpdates)
    }

    fun fetchAllUsers(): Observable<List<User>> {
        return RxFirebaseDatabase
                .observeSingleValueEvent(database.child(USERS), DataSnapshotMapperAutoId.listOf(User::class.java))
    }

    //***********************CHAT*******************************************************************
    //**********************************************************************************************
    fun createPrivateChat(context: Context, recipient: User): String {
        val chatId = HelpFunctions.getPrivateChatId(recipient.id)

        val childUpdates = HashMap<String, Any>()
        childUpdates["/" + CHATS + "/" + chatId + "/members/" + auth.currentUser!!.uid] = true
        childUpdates["/" + CHATS + "/" + chatId + "/members/" + recipient.id] = true
        childUpdates["/" + CHATS + "/" + chatId + "/users/" + auth.currentUser!!.uid + "/name"] = HelpFunctions.getCurrentUserFullName(context)
        childUpdates["/" + CHATS + "/" + chatId + "/users/" + auth.currentUser!!.uid + "/timestamp"] = Date().time
        childUpdates["/" + CHATS + "/" + chatId + "/users/" + recipient.id + "/name"] = recipient.firstName + " " + recipient.lastName
        childUpdates["/" + CHATS + "/" + chatId + "/users/" + recipient.id + "/timestamp"] = Date().time

        database.updateChildren(childUpdates)
        return chatId
    }

    fun fetchChats(): Observable<List<Chat>> {
        return RxFirebaseDatabase
                .observeSingleValueEvent(getChatsRef(), DataSnapshotMapperAutoId.listOf(Chat::class.java))
    }

    fun fetchChatsEvent(): Observable<RxFirebaseChildEvent<Chat>> {
        return RxFirebaseDatabase
                .observeChildEvent(getChatsRef(), Chat::class.java)
    }

    private fun getChatsRef(): Query {
        return database.child(CHATS).orderByChild(String.format(Locale.getDefault(), "%s/%s", MEMBERS, auth.currentUser!!.uid)).equalTo(true)
    }

    fun removeChat(chatId: String) {
        database.child(CHATS).child(chatId).removeValue()
    }

    fun getMessagesRef(chatId: String): DatabaseReference {
        return database.child(MESSAGES).child(chatId)
    }

    fun createPrivateMessage(comment: Comment, recipient: User) {
        val chatId = getPrivateChatId(recipient.id)

        val key = database.child(MESSAGES).push().key
        val childUpdates = HashMap<String, Any>()
        childUpdates["/$MESSAGES/$chatId/$key/"] = comment
        childUpdates["/" + CHATS + "/" + chatId + "/members/" + auth.currentUser!!.uid] = true
        childUpdates["/" + CHATS + "/" + chatId + "/members/" + recipient.id] = true
        database.updateChildren(childUpdates)
    }
}
