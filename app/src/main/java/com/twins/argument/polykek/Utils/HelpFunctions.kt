package com.twins.argument.polykek.Utils

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.twins.argument.polykek.Application.Companion.auth
import com.twins.argument.polykek.R
import com.twins.argument.polykek.models.Chat
import com.twins.argument.polykek.models.Lesson
import com.twins.argument.polykek.models.User
import java.util.*

class HelpFunctions {
    companion object {

        private const val SHARED_PREF_NAME = "com.phoenix.kspt"

        private fun getSharedPref(context: Context): SharedPreferences {
            return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        }

        fun getSharedPrefEditor(context: Context): SharedPreferences.Editor {
            return context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit()
        }

        fun getCurrentUserId(context: Context): String {
            return getSharedPref(context).getString(USER_ID, "User")
        }

        fun getCurrentUserFirstName(context: Context): String {
            return getSharedPref(context).getString(USER_FIRST_NAME, "User")
        }

        fun getCurrentUserLastName(context: Context): String {
            return getSharedPref(context).getString(USER_LAST_NAME, "User")
        }

        fun getCurrentUserEmail(context: Context): String {
            return getSharedPref(context).getString(USER_EMAIL, "user@gmail.com")
        }

        fun getCurrentUserAvatar(context: Context): String {
            return getSharedPref(context).getString(USER_AVATAR, User.avatar_default)
        }

        fun getCurrentUserGroup(context: Context): String {
            return getSharedPref(context).getString(USER_GROUP, "9000")
        }


        fun getCurrentUserFullName(context: Context): String {
            return getSharedPref(context).getString(USER_FIRST_NAME, "User") + " " +
                    getSharedPref(context).getString(USER_LAST_NAME, "")
        }

        fun getCurrentUserStatus(context: Context): String {
            return getSharedPref(context).getString(USER_EMAIL, "User@mail.com")
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        fun initToolbarSettings(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                val background = ContextCompat.getDrawable(activity, R.color.colorPrimaryDark)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(activity, android.R.color.transparent)
                window.navigationBarColor = ContextCompat.getColor(activity, android.R.color.transparent)
                window.setBackgroundDrawable(background)
            }
        }

        fun getFirstLettersUpperCase(firstName: String, lastName: String): String {
            return when{
                firstName.isNotEmpty() && lastName.isNotEmpty() -> (firstName[0].toString() + lastName[0].toString()).toUpperCase()
                firstName.isNotEmpty() && lastName.isEmpty() -> firstName[0].toString().toUpperCase()
                firstName.isEmpty() && lastName.isNotEmpty() -> lastName[0].toString().toUpperCase()
                else -> "NEMO"
            }
        }

        private fun getFirstLetter(string: String): String {
            return string[0].toString().toLowerCase()
        }

        fun getBackgroundColorFor(fullName: String): Int {
            val nameParts = fullName.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val alphabet = "abcdefghijklmnopqrstuvwxyzабвгдеёжзиклмнопрстуфхцчшщъыьэюя"

            var code1 = 0
            var code2 = 0

            if (nameParts.isNotEmpty() && nameParts[0].isNotEmpty())
                code1 = alphabet.indexOf(getFirstLetter(nameParts[0]))

            if (nameParts.size > 1 && nameParts[1].isNotEmpty())
                code2 = alphabet.indexOf(getFirstLetter(nameParts[1]))

            val colorCode = (code1 + code2) % 5

            when (colorCode) {
                0 -> return R.color.letterBackground1
                1 -> return R.color.letterBackground0
                2 -> return R.color.letterBackground2
                3 -> return R.color.letterBackground3
                4 -> return R.color.letterBackground4
            }

            return R.color.letterBackground2
        }

        val random = Random()
        fun rand(from: Int, to: Int) : Int {
            return random.nextInt(to - from) + from
        }

        fun getRandomLesson(time: String): Lesson {
            val lessonsName = arrayOf("Математический анализ", "Вычмат", "Начертательная геометрия",
                "Русский язык", "Английский язык", "Разработка ПО", "Сети", "Kotlin", "C++", "Java",
                "Вышмат", "Французский язык", "Музыка", "Материаловедение", "Java", "Компьютерная безопасность",
                "Оптимизация машин", "Неройнные сети", "История науки", "Схемотехника",
                "Аппаратная разработка", "Базы данных", "Экономика", "Философия", "Френология")

            val addresses = arrayOf("ГЗ. ауд.", "Корп. 1 ауд.", "Корп. 2 ауд.", "Корп. 3 ауд.", "Корп. 4 ауд."
                    , "Корп. 5 ауд.", "Корп. 6 ауд.", "Корп. 7 ауд.", "Корп. 8 ауд.", "Корп. 9 ауд.")

            val teachers = arrayOf("Глухих М.А.", "Ицыксон В.М.", "Жвариков В.А.", "Жуков В.В.", "Илон М.А",
                    "Адова С.М.", "Шелухов А.А.", "Павлоский Я.В.", "Ичкерский З.В.", "Чеботарев М.М.",
                    "Хутарной Я.В.", "Антонов А.А.", "Яковлев К.С.", "Филиппов П.Ф.", "Игнатенко Э.А.",
                    "Ломова Т.В.", "Невиданный К.К.", "Ломоносов М.Ю.")
            val lesson = Lesson()
            lesson.lessonName = lessonsName[rand(0, lessonsName.size - 1)]
            lesson.address = addresses[rand(0, addresses.size - 1)] + rand(0, 150)
            lesson.teacherName = teachers[rand(0, teachers.size - 1)]
            lesson.time = time
            return lesson
        }

        fun getPrivateChatId(recipient: String): String {
            val compare = recipient.compareTo(auth.currentUser!!.uid)
            return when {
                compare < 0 -> String.format(Locale.getDefault(), "%s_%s", recipient, auth.currentUser!!.uid)
                compare > 0 -> String.format(Locale.getDefault(), "%s_%s", auth.currentUser!!.uid, recipient)
                else -> "chatId-zombie"
            }
        }

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager = activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, 0)
        }

        fun getChatName(chat: Chat, currentUserId: String): String {
            for (user in chat.users.values)
                if (user.id != currentUserId)
                    return user.getFullName()

            return "Messages"
        }
    }
}