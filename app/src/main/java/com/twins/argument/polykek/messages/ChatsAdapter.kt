package com.twins.argument.polykek.messages

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.twins.argument.polykek.models.Chat

import java.util.ArrayList
import java.util.HashMap

interface ChatItemListener{
    fun openChat(chatId: String)
    fun removeChat(chatId: String)
}

class ChatsAdapter(context: Context?, private var items: ArrayList<Chat>,
                   private val listener: ChatItemListener) : RecyclerView.Adapter<ChatViewHolderItem>() {

    private var itemsMap: HashMap<String, Chat> = HashMap()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onBindViewHolder(holder: ChatViewHolderItem, position: Int) {
        holder.bindTo(items[position])
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ChatViewHolderItem {
        return ChatViewHolderItem.create(inflater, viewGroup, listener)
    }

    fun getItem(chatId: String): Chat{
        return itemsMap[chatId]!!
    }

    fun addNewChat(chat: Chat) {
        if(chatIsNew(chat.id)) {
            itemsMap[chat.id] = chat
            items = ArrayList(itemsMap.values)

            this.notifyDataSetChanged()
        }
    }

    private fun chatIsNew(id: String): Boolean{
        return !itemsMap.containsKey(id)
    }

    fun updateChat(chat: Chat) {
        itemsMap[chat.id] = chat
        items = ArrayList(itemsMap.values)
        this.notifyDataSetChanged()
    }

    fun removeChat(chatId: String){
        itemsMap.remove(chatId)
        items = ArrayList(itemsMap.values)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

}
