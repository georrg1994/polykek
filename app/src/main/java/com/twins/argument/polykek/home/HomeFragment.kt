package com.twins.argument.polykek.home


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.twins.argument.polykek.Application.Companion.firebase
import com.twins.argument.polykek.MainWindowInterface

import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.*
import com.twins.argument.polykek.firebase.FirebaseFragment
import com.twins.argument.polykek.messages.ChatListActivity
import com.twins.argument.polykek.models.User
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : FirebaseFragment() {
    private var user: User = User()
    private var listener: MainWindowInterface? = null

    companion object {
        fun newInstance(user: User): Fragment{
            val fragment = HomeFragment()
            val bundle = Bundle()
            bundle.putParcelable(USER,  user)
            fragment.arguments = bundle
            return fragment
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.setTitle("")

        if(arguments != null) {
            user = arguments!!.getParcelable(USER)
            initFields()
        } else {
            firebase.fetchUserById(HelpFunctions.getCurrentUserId(context!!)).subscribe {
                    user = it
                    user.id = auth.currentUser!!.uid
                    initFields()
                }
        }
    }

    private fun initFields(){
        bigNameField.text = "${user.firstName} ${user.lastName}"
        fullNameField.setText("${user.firstName} ${user.lastName}")
        emailField.setText(user.email)
        groupIdField.setText(user.groupId)
        avatar.setImageURI(user.avatar)
        studentLvlField.setText(user.studyingWordLvl)
        formOfStudingField.setText(user.studyingForm)
        courseNumberField.setText(user.studyingNumberLvl)
        universityField.setText(user.university)
        leaningDirectionField.setText(user.studyingDirection)
        directivityField.setText(user.studyingDirectionProfile)
        departmentField.setText(user.department)

        // listener
        savekButton.setOnClickListener{ saveUpdates() }

        if(user.id != HelpFunctions.getCurrentUserId(context!!)){
            setEnable(false)
            sendMessage.setOnClickListener{ ChatListActivity.startActivity(context!!, user) }

        } else {
            setEnable(true)
        }
        listener!!.visibilityWait(false)
    }

    private fun setEnable(enable: Boolean){
        bigNameField.isEnabled = enable
        fullNameField.isEnabled = enable
        emailField.isEnabled = enable
        groupIdField.isEnabled = enable
        avatar.isEnabled = enable
        studentLvlField.isEnabled = enable
        formOfStudingField.isEnabled = enable
        courseNumberField.isEnabled = enable
        universityField.isEnabled = enable
        leaningDirectionField.isEnabled = enable
        directivityField.isEnabled = enable
        departmentField.isEnabled = enable
        savekButton.visibility = if(enable) View.VISIBLE else View.GONE
        sendMessage.visibility = if(enable) View.GONE else View.VISIBLE
    }

    private fun saveUpdates(){
        val user = User()
        user.id = HelpFunctions.getCurrentUserId(context!!)

        val array = fullNameField.text.toString().trim().split(" ")
        if(array.isNotEmpty())
            user.firstName = array[0]

        if(array.size > 1)
            user.lastName = array[1]

        user.avatar = this.user.avatar
        user.email = emailField.text.toString()
        user.groupId = groupIdField.text.toString()
        user.studyingWordLvl = studentLvlField.text.toString()
        user.studyingForm = formOfStudingField.text.toString()
        user.studyingNumberLvl = courseNumberField.text.toString()
        user.university = universityField.text.toString()
        user.studyingDirection = leaningDirectionField.text.toString()
        user.studyingDirectionProfile = directivityField.text.toString()
        user.department = departmentField.text.toString()

        firebase.pushNewUser(user)

        Toast.makeText(context, "Профиль успешно изменен", Toast.LENGTH_SHORT).show()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(listener == null && context is MainWindowInterface)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
