package com.twins.argument.polykek.people


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ImageView
import com.twins.argument.polykek.Application.Companion.firebase
import com.twins.argument.polykek.MainWindowInterface

import com.twins.argument.polykek.R
import com.twins.argument.polykek.home.HomeFragment
import com.twins.argument.polykek.models.User
import kotlinx.android.synthetic.main.fragment_lessons_list.*
import java.util.ArrayList

class PeopleFragment : Fragment(), SearchView.OnQueryTextListener {
    private lateinit var adapter: PeopleAdapter
    private var allUsers: ArrayList<User> = ArrayList()
    private var query: String = ""
    private var listener: MainWindowInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_people, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.setTitle(getString(R.string.stuff))
        listener?.visibilityWait(true)

        firebase.fetchAllUsers().subscribe{
            if(it != null) {
                allUsers.clear()
                allUsers.addAll(it)
                listener?.visibilityWait(false)
                setupRecyclerView(allUsers)
            }
        }
    }

    private fun setupRecyclerView(users: ArrayList<User>){
        adapter = PeopleAdapter(context!!, users, object : ClickToProfile{
            override fun openProfile(user: User) {
                listener?.replaceFragment(HomeFragment.newInstance(user), true)
            }
        })
        recyclerView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_people, menu)

        // Search setting
        val searchMenuItem = menu.findItem(R.id.search)
        val searchView = searchMenuItem.actionView as SearchView

        // set colors and hint text
        val searchAutoComplete = searchView.findViewById<SearchView.SearchAutoComplete>(R.id.search_src_text)
        searchAutoComplete.setHintTextColor(ContextCompat.getColor(context!!, R.color.disableColor))
        searchAutoComplete.setTextColor(ContextCompat.getColor(context!!, R.color.textColor))
        searchAutoComplete.hint = "Поиск..."

        // Color for searchField background
        val searchPlate = searchView.findViewById<View>(R.id.search_plate)
        searchPlate.setBackgroundResource(R.drawable.rounded_white_rectangle)

        // set close image
        val searchCloseIcon = searchView.findViewById<ImageView>(R.id.search_close_btn)
        searchCloseIcon.setImageResource(R.drawable.ic_close_black_24dp)

        searchView.setOnQueryTextListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onQueryTextSubmit(query: String?): Boolean { return true }

    override fun onQueryTextChange(query: String?): Boolean {
        val filteredUsers = ArrayList<User>()

            if (query!!.isEmpty()) {
                filteredUsers.addAll(allUsers)

            } else {
                this.query = query.toLowerCase()

                for (user in allUsers) {
                    val text = ( user.firstName + user.lastName +
                            user.userStatus + user.groupId).toLowerCase()

                    if (text.contains(query))
                        filteredUsers.add(user)
                }
            }

            setupRecyclerView(filteredUsers)

        return true
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(listener == null && context is MainWindowInterface)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
