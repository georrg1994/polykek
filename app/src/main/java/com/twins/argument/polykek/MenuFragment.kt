package com.twins.argument.polykek

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.twins.argument.polykek.Utils.HelpFunctions
import kotlinx.android.synthetic.main.fragment_menu.*

interface MenuListener {
    fun showHome()
    fun showSchedulers()
    fun showNotificationsList()
    fun showStuff()
    fun logout()
}

private const val POSITION_HOME = 0
private const val POSITION_SCHEDULERS = 1
private const val POSITION_NOTIFICATIONS_LIST = 2
private const val POSITION_STUFF = 3
private const val POSITION_LOGOUT = 4

class MenuFragment : Fragment() {
    private var listener: MenuListener? = null

    private var currentPosition: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameField.text = HelpFunctions.getCurrentUserFullName(context!!)
        statusUserField.text = getString(R.string.student_and_group) + HelpFunctions.getCurrentUserGroup(context!!)

        val firstLettersInName = HelpFunctions.getFirstLettersUpperCase(
                HelpFunctions.getCurrentUserFirstName(context!!),
                HelpFunctions.getCurrentUserLastName(context!!))

        val backgroundColor = HelpFunctions.getBackgroundColorFor(
                HelpFunctions.getCurrentUserFullName(context!!))

        val bgShape = avatar.background as GradientDrawable
        bgShape.setColor(ContextCompat.getColor(context!!, backgroundColor))
        avatar.text = firstLettersInName

        setupNavigationViewClickListeners()
    }

    private fun setupNavigationViewClickListeners() {
        navView.setNavigationItemSelectedListener{
            when (it.itemId) {
                R.id.drawer_home -> {
                    listener?.showHome()
                    currentPosition = POSITION_HOME
                }
                R.id.drawer_stuff -> {
                    listener?.showStuff()
                    currentPosition = POSITION_STUFF
                }
                R.id.drawer_schedulers_list -> {
                    listener?.showSchedulers()
                    currentPosition = POSITION_SCHEDULERS
                }
                R.id.drawer_notification -> {
                    listener?.showNotificationsList()
                    currentPosition = POSITION_NOTIFICATIONS_LIST
                }
                R.id.drawer_logout -> {
                    listener?.logout()
                    currentPosition = POSITION_LOGOUT
                }
            }
            true
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MenuListener)
            listener = context
        else
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
