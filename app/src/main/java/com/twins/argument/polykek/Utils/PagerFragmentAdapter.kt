package com.twins.argument.polykek.Utils

/**
 * Created by darkt on 1/4/2018.
 */
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.util.Pair

import java.util.ArrayList

class PagerFragmentAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    private val mFragmentTitlePairList: MutableList<Pair<Fragment, String>>

    init {
        mFragmentTitlePairList = ArrayList()
    }

    override fun getItem(position: Int): Fragment {
        val currentItem = mFragmentTitlePairList[position]
        return currentItem.first!!
    }

    override fun getCount(): Int {
        return mFragmentTitlePairList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        val currentFragment = mFragmentTitlePairList[position]
        return currentFragment.second!!
    }

    fun addFragmentTitlePair(fragment: Fragment, title: String) {
        mFragmentTitlePairList.add(Pair(fragment, title))
    }

    fun addFragmentTitlePair(index: Int, fragment: Fragment, title: String) {
        if (mFragmentTitlePairList.size > index)
            mFragmentTitlePairList[index] = Pair(fragment, title)
        else
            mFragmentTitlePairList.add(Pair(fragment, title))
    }
}