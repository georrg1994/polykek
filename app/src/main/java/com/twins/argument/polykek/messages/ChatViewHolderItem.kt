package com.twins.argument.polykek.messages

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.twins.argument.polykek.Utils.HelpFunctions
import com.twins.argument.polykek.databinding.ItemChatBinding
import com.twins.argument.polykek.models.Chat
import java.text.SimpleDateFormat
import java.util.*


class ChatViewHolderItem private constructor(private val binding: ItemChatBinding,
                                             private val listener: ChatItemListener) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun bindTo(chat: Chat) {
        binding.avatar.setImageURI(chat.avatar)
        binding.nameField.text = HelpFunctions.getChatName(chat, HelpFunctions.getCurrentUserId(binding.root.context))
        binding.time.text = SimpleDateFormat("MM/dd/yyyy").format(Date(chat.time))
        binding.lastMessage.text = chat.lastMessage

        // listeners
        binding.root.setOnClickListener{ listener.openChat(chat.id) }
        binding.root.setOnLongClickListener{
            listener.removeChat(chat.id)
            true
        }
        binding.executePendingBindings()
    }

    companion object {
        fun create(inflater: LayoutInflater,
                   parent: ViewGroup, listener: ChatItemListener): ChatViewHolderItem {
            val binding = ItemChatBinding.inflate(inflater, parent, false)
            return ChatViewHolderItem(binding, listener)
        }
    }
}