package com.twins.argument.polykek.schedullers

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.LIST_OF_LESSONS
import com.twins.argument.polykek.models.Lesson
import kotlinx.android.synthetic.main.fragment_lessons_list.*
import java.util.*


/**
 * Created by darkt on 8/24/2017.
 */
class FragmentListOfLessons : Fragment(){
    private lateinit var adapter: LessonsAdapter
    private var lessons: ArrayList<Lesson> = ArrayList()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lessons_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(arguments != null)
            lessons = arguments!!.getParcelableArrayList<Lesson>(LIST_OF_LESSONS)
        adapter = LessonsAdapter(context!!, lessons)
        recyclerView.adapter = adapter
    }
}