package com.twins.argument.polykek

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import com.gmail.samehadar.iosdialog.IOSDialog
import com.twins.argument.polykek.Utils.HelpFunctions
import com.twins.argument.polykek.Utils.USER
import com.twins.argument.polykek.authorization.ActivitySignIn
import com.twins.argument.polykek.firebase.FireBaseActivity
import com.twins.argument.polykek.home.HomeFragment
import com.twins.argument.polykek.messages.ChatListActivity
import com.twins.argument.polykek.models.User
import com.twins.argument.polykek.people.PeopleFragment
import com.twins.argument.polykek.schedullers.SchedulersFragment
import kotlinx.android.synthetic.main.activity_main.*


interface MainWindowInterface{
    fun setTitle(title: String)
    fun replaceFragment(fragment: Fragment, stackEnable: Boolean)
    fun visibilityWait(enable: Boolean)
}

class MainActivity : FireBaseActivity(), MenuListener, MainWindowInterface {
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var menuFragment: MenuFragment
    private lateinit var currentUser: User
    private lateinit var dialog: IOSDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initDialog()
        initDrawer()

        visibilityWait(true)
        showHome()
    }

    private fun initDialog(){
        dialog = IOSDialog.Builder(this)
                .setSpinnerColorRes(R.color.disableColor)
                .setTitle(getString(R.string.pleaseWait))
                .setTitleColorRes(R.color.disableColor)
                .setCancelable(true)
                .setMessageContentGravity(Gravity.END)
                .build()
    }

    override fun replaceFragment(fragment: Fragment, stackEnable: Boolean){
        drawerLayout.closeDrawers()
        val tr = supportFragmentManager.beginTransaction()
        tr.setCustomAnimations(R.anim.right_animation_enter, R.anim.right_animation_leave)
        tr.replace(R.id.mainContainer, fragment, fragment.tag)
        if(stackEnable)
            tr.addToBackStack(null)
        tr.commit()
    }

    private fun initDrawer() {
        menuFragment = MenuFragment()
        supportFragmentManager.beginTransaction()
                .replace(R.id.menuContainer, menuFragment, menuFragment.tag)
                .commit()

        drawerToggle = object : ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close){}
        drawerToggle.isDrawerIndicatorEnabled = true
    }

    override fun logout() {
        auth.signOut()
        HelpFunctions.getSharedPrefEditor(this).clear().apply()

        val intent = Intent(this, ActivitySignIn::class.java)
        val options = ActivityOptions.makeCustomAnimation(this, R.anim.fade_in, R.anim.fade_out)
        startActivity(intent, options.toBundle())
        finish()
    }

    override fun showHome() {
        supportActionBar!!.title = ""
        replaceFragment(HomeFragment(), true)
    }

    override fun showSchedulers() {
        replaceFragment(SchedulersFragment(), true)

    }

    override fun showNotificationsList() {
        drawerLayout.closeDrawers()
        ChatListActivity.startActivity(this)
    }

    override fun showStuff() {
        replaceFragment(PeopleFragment(), true)
    }

    override fun visibilityWait(enable: Boolean) {
        if(enable) {
            waitProgressbar.visibility = View.VISIBLE
            dialog.show()
        } else {
            waitProgressbar.visibility = View.GONE
            dialog.hide()
        }
    }

    override fun setTitle(title: String) {
        supportActionBar!!.title = title
    }

}
