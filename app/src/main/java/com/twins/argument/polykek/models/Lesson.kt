package com.twins.argument.polykek.models

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Lesson() : ParentItem(){
    var lessonName: String = "Урок 1"
    var teacherName: String = "Глухих М.В."
    var time: String = "10:00-11:30"
    var address: String = "ГЗ. 101 ауд."

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        lessonName = parcel.readString()
        teacherName = parcel.readString()
        time = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(lessonName)
        dest.writeString(teacherName)
        dest.writeString(time)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Lesson> {
        override fun createFromParcel(parcel: Parcel): Lesson {
            return Lesson(parcel)
        }

        override fun newArray(size: Int): Array<Lesson?> {
            return arrayOfNulls(size)
        }
    }
}