package com.twins.argument.polykek.people

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.twins.argument.polykek.databinding.ItemPeopleBinding
import com.twins.argument.polykek.models.User


class PeopleViewHolderItem private constructor(private val binding: ItemPeopleBinding,
                                               private val clickToProfile: ClickToProfile) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n")
    fun bindTo(user: User) {
        binding.avatar.setImageURI(user.avatar)
        binding.nameField.text = user.firstName + " " + user.lastName
        binding.groupIdField.text = user.groupId
        binding.peopleStatusField.text = user.userStatus

        // listener
        binding.moreBtn.setOnClickListener{ clickToProfile.openProfile(user) }
        binding.executePendingBindings()
    }

    companion object {
        fun create(inflater: LayoutInflater,
                   parent: ViewGroup, clickToProfile: ClickToProfile): PeopleViewHolderItem {
            val binding = ItemPeopleBinding.inflate(inflater, parent, false)
            return PeopleViewHolderItem(binding, clickToProfile)
        }
    }
}