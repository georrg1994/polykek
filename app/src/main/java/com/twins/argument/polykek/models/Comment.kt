package com.twins.argument.polykek.models

import com.google.firebase.database.IgnoreExtraProperties

import java.util.Date

@IgnoreExtraProperties
class Comment() : ParentItem() {
    var userId: String? = null
    var message = "zombie message"
    var timestamp = Date().time
    var name = ""

    constructor(userId: String, userName: String, message: String, timestamp: Long): this() {
        this.userId = userId
        this.name = userName
        this.message = message
        this.timestamp = timestamp
    }
}
