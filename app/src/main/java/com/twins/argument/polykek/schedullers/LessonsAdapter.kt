package com.twins.argument.polykek.schedullers

/**
 * Created by darkt on 1/9/2018.
 */

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.twins.argument.polykek.models.Lesson


class LessonsAdapter(context: Context, private var items: MutableList<Lesson>) :
        RecyclerView.Adapter<LessonViewHolderItem>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onBindViewHolder(viewHolder: LessonViewHolderItem, position: Int) {
        viewHolder.bindTo(items[position])
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): LessonViewHolderItem {
        return LessonViewHolderItem.create(inflater, viewGroup)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addTarget(target: Lesson) {
        items.add(target)
        this.notifyDataSetChanged()
    }
}