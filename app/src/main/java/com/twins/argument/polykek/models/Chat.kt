package com.twins.argument.polykek.models

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Chat() : ParentItem() {
    var avatar: String = ""
    var users: Map<String, User> = HashMap()
        set(users) {
            field = users

            for ((key, value) in this.users) {
                value.id = key
            }
        }
    var lastMessage: String? = null
    var time: Long = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        avatar = parcel.readString()
        lastMessage = parcel.readString()
        time = parcel.readLong()

        val size = parcel.readInt()
        this.users = HashMap(size)
        for(i in 0 until size){
            val key = parcel.readString()
            val user: User = parcel.readValue(User::class.java.classLoader) as User
            (users as HashMap<String, User>)[key] = user
        }
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(id)
        dest.writeString(avatar)
        dest.writeString(lastMessage)
        dest.writeLong(time)

        dest.writeInt(users.size)
        for(item in users){
            dest.writeString(item.key)
            dest.writeParcelable(item.value, flags)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Chat> {
        override fun createFromParcel(parcel: Parcel): Chat {
            return Chat(parcel)
        }

        override fun newArray(size: Int): Array<Chat?> {
            return arrayOfNulls(size)
        }
    }
}
