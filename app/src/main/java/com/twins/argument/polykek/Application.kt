package com.twins.argument.polykek

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.auth.FirebaseAuth
import com.twins.argument.polykek.firebase.FirebaseHelper
import com.twins.argument.polykek.models.User

class Application : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var mContext: Context
        lateinit var firebase: FirebaseHelper
        lateinit var auth: FirebaseAuth
        lateinit var currentUser: User
    }

    override fun onCreate() {
        super.onCreate()
        firebase = FirebaseHelper()
        auth = FirebaseAuth.getInstance()
        Fresco.initialize(applicationContext)
        currentUser = User()

        mContext = this
    }
}