package com.twins.argument.polykek.messages

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.twins.argument.polykek.R

class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    internal val senderAvatarView: TextView = itemView.findViewById(R.id.avatar)
    internal val messageView: TextView = itemView.findViewById(R.id.message)
    internal val timestampView: TextView = itemView.findViewById(R.id.time)
    internal val titleName: TextView = itemView.findViewById(R.id.nameField)
}