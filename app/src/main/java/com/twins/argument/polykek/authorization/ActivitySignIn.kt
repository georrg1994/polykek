package com.twins.argument.polykek.authorization

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.github.clans.fab.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.twins.argument.polykek.MainActivity
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.*
import com.twins.argument.polykek.firebase.FireBaseActivity
import kotlinx.android.synthetic.main.activity_sign_in.*

class ActivitySignIn : FireBaseActivity() {
    enum class TypeOfFabs {SIGN_UP, SIGN_IN, CHANGE_PASSWORD}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        auth = FirebaseAuth.getInstance()
        addFabToMenu("Регистрация", R.drawable.ic_person_add_24dp, TypeOfFabs.SIGN_UP)
        addFabToMenu("Изменить пароль", R.drawable.ic_change_password_24dp, TypeOfFabs.CHANGE_PASSWORD)
        addFabToMenu("Войти", R.drawable.ic_arrow_forward_24dp, TypeOfFabs.SIGN_IN)
        fabMenu.getChildAt(1).isEnabled = false
    }


    override fun onStart() {
        super.onStart()

        // Check auth on Activity start
        if (auth.currentUser != null) {
            val intent = Intent(this@ActivitySignIn, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun changePass(){

    }

    private fun signUp(){
        val intent = Intent(this, ActivitySignUp::class.java)
        val options = ActivityOptions.makeCustomAnimation(this, R.anim.right_animation_enter, R.anim.right_animation_leave)
        startActivity(intent, options.toBundle())
    }

    private fun signIn() {
        val email = emailFieldView.text.toString().trim()
        val password = passwordFieldView.text.toString()

        if (!checkValidateForm(email, password))
            return

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, { task ->
            if (task.isSuccessful)
                onAuthSuccess()
            else
                Toast.makeText(this, "Ошибка авторизации", Toast.LENGTH_SHORT).show()
        })
    }

    private fun checkValidateForm(email: String, password: String): Boolean {
        if (email == "") {
            Toast.makeText(this, "Введите email", Toast.LENGTH_SHORT).show()
            emailFieldView.error = "неверное имя"
            return false
        }

        if (password == ""){
            Toast.makeText(this, "Введите пароль", Toast.LENGTH_SHORT).show()
            passwordFieldView.error = "неверный пароль"
            return false
        }

        emailFieldView.error = null
        passwordFieldView.error = null

        return true
    }

    private fun onAuthSuccess() {
        firebase.fetchUserById(auth.currentUser?.uid!!).first().subscribe({ user ->

            if(user != null) {
                user.id = auth.currentUser?.uid!!
                val sharedPrefEditor = HelpFunctions.getSharedPrefEditor(this)
                sharedPrefEditor.putString(USER_ID, auth.currentUser?.uid!!)
                sharedPrefEditor.putString(USER_FIRST_NAME, user.firstName)
                sharedPrefEditor.putString(USER_LAST_NAME, user.lastName)
                sharedPrefEditor.putString(USER_EMAIL, user.email)
                sharedPrefEditor.putString(USER_GROUP, user.groupId)
                sharedPrefEditor.putString(USER_AVATAR, user.avatar)
                sharedPrefEditor.apply()

                // Go to MainActivity
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(USER, user)
                val options = ActivityOptions.makeCustomAnimation(this, R.anim.abc_fade_in, R.anim.abc_fade_out)
                startActivity(intent,options.toBundle())
                finish()
            } else {
                Toast.makeText(this, "В базе о Вас нет информации", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun addFabToMenu(name: String, imageId: Int, type: TypeOfFabs) {
        // Sign in
        val programFab = FloatingActionButton(this)
        programFab.buttonSize = FloatingActionButton.SIZE_MINI
        programFab.labelText = name
        programFab.colorNormal = ContextCompat.getColor(this, R.color.colorAccent)
        programFab.setImageResource(imageId)
        fabMenu.addMenuButton(programFab)

        when(type){
            TypeOfFabs.SIGN_UP -> programFab.setOnClickListener { signUp() }
            TypeOfFabs.SIGN_IN -> programFab.setOnClickListener { signIn() }
            TypeOfFabs.CHANGE_PASSWORD -> programFab.setOnClickListener { changePass() }
        }
    }
}
