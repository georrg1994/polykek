package com.twins.argument.polykek.messages


import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.Query
import com.twins.argument.polykek.R
import com.twins.argument.polykek.Utils.HelpFunctions
import com.twins.argument.polykek.models.Comment
import java.text.SimpleDateFormat
import java.util.*


class CommentsAdapter(ref: Query) :
        FirebaseRecyclerAdapter<Comment, CommentViewHolder>(Comment::class.java, R.layout.item_comment,
                CommentViewHolder::class.java, ref) {

    private val mDateFormat = SimpleDateFormat(DATE_TIME_FORMAT, Locale.getDefault())


    override fun populateViewHolder(viewHolder: CommentViewHolder, comment: Comment, position: Int) {
        viewHolder.messageView.text = comment.message
        val formatTimestamp = mDateFormat.format(Date(comment.timestamp))
        viewHolder.timestampView.text = String.format(Locale.getDefault(), "%s", formatTimestamp)
        val array = comment.name.trim().split(" ")
        var firstName = ""
        var lastName = ""
        if(array.isNotEmpty())
            firstName = array[0]
        if(array.size > 1)
            lastName = array[1]

        viewHolder.senderAvatarView.text = HelpFunctions.getFirstLettersUpperCase(firstName, lastName)
        viewHolder.titleName.text =  comment.name

        val backgroundColor = HelpFunctions.getBackgroundColorFor(comment.name)
        val bgShape = viewHolder.senderAvatarView.background as GradientDrawable
        bgShape.setColor(ContextCompat.getColor(viewHolder.itemView.context, backgroundColor))
    }

    override fun parseSnapshot(snapshot: DataSnapshot): Comment {
        val comment = snapshot.getValue(Comment::class.java)
        comment!!.id = snapshot.key!!
        return comment
    }

    companion object {
        private val DATE_TIME_FORMAT = "dd MMM HH:mm"
    }
}
