package com.twins.argument.polykek.schedullers


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.twins.argument.polykek.MainWindowInterface

import com.twins.argument.polykek.R
import com.twins.argument.polykek.firebase.FirebaseFragment
import kotlinx.android.synthetic.main.fragment_schedulers.*
import java.util.*


class SchedulersFragment : FirebaseFragment() {
    private var currentWeekNumber = 0
    private var listener: MainWindowInterface? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_schedulers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.setTitle("Расписание")

        val calendar = Calendar.getInstance()
        currentWeekNumber = calendar.get(Calendar. WEEK_OF_YEAR)
        replaceFragment(FragmentDayLessons())

        nextBtn.setOnClickListener{
            currentWeekNumber--
            replaceFragment(FragmentDayLessons())
        }

        previousBtn.setOnClickListener{
            currentWeekNumber++
            replaceFragment(FragmentDayLessons())
        }
    }

    private fun replaceFragment(fragment: Fragment){
            fragmentManager!!.beginTransaction()
                    .setCustomAnimations(R.anim.right_animation_enter, R.anim.right_animation_leave)
                    .replace(R.id.container, fragment, fragment.tag)
                    .commit()

        if(currentWeekNumber % 2 == 0)
            weekNumber.text = "I неделя"
        else
            weekNumber.text = "II неделя"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(listener == null && context is MainWindowInterface)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
