package com.twins.argument.polykek.firebase

import android.support.v4.app.Fragment
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.twins.argument.polykek.Application

open class FirebaseFragment : Fragment(){
    private lateinit var fdbHelper: FirebaseHelper
    protected lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fdbHelper = Application.firebase
        auth = Application.auth
    }
}
